package main

import (
	"github.com/stretchr/testify/mock"
	"sync"
	"testing"
	"testing/goroutines-mocking/mocks"
)

func Test_runPromise(t *testing.T) {

	m := mock.Mock{}
	m.On("ConcurrentFunc").Return(true)

	wg := &sync.WaitGroup{}

	tests := []struct {
		name string
		p    Promise
	}{
		{"Promise1", &mocks.Promise{m, wg}},
	}

	for _, tt := range tests {
		wg.Add(1)
		t.Run(tt.name, func(t *testing.T) {
			runPromise(tt.p)
		})
	}

	wg.Wait()
}