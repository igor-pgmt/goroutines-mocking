package main

import "time"
import "fmt"

func main() {
	runPromise(&p{})
}

type p struct{}

func (_ *p) ConcurrentFunc() bool {
	time.Sleep(1)
	return true
}

type Promise interface {
	ConcurrentFunc() bool
}

func runPromise(p Promise) {
	fmt.Println(p.ConcurrentFunc())
}
